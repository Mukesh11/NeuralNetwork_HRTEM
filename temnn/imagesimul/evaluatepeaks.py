from stm.preprocess import normalize
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
import numpy as np

def precision_recall(predicted, target, sampling):
    """Precision and recall for peak positions"""
    # Precision: Number of correctly predicted peaks 
    # divided by number of target peaks
    singleclass = predicted.shape[1] == 2
    distance=0.5/sampling
    if len(predicted) == 0:
        if singleclass:
            return (0.0, 1.0)
        else:
            return (0.0, 1.0, 0.0, 0.0)
    if len(target) == 0:
        if singleclass:
            return (1.0, 0.0)
        else:
            return (1.0, 0.0, 0.0, 0.0)
    tree = KDTree(target)
    x = tree.query(predicted, distance_upper_bound=distance)[0]
    precision = (x <= distance).sum() / len(predicted)
    # Recall: Number of target peaks that were found
    # divided by total number of target peaks
    tree = KDTree(predicted)
    x = tree.query(target, distance_upper_bound=distance)[0]
    recall = (x <= distance).sum() / len(target)
    if singleclass:
        # Only one class, just return precision and recall
        return (precision, recall)
    else:
        # Multiple classes, also identify peaks that are placed
        # correctly but in the wrong class.
        assert predicted.shape[1] == 3
        tree = KDTree(target[:,:2]) # Remove info about layer.
        x = tree.query(predicted[:,:2], distance_upper_bound=distance)[0]
        p2 = (x <= distance).sum() / len(predicted)
        tree = KDTree(predicted[:,:2])
        x = tree.query(target[:,:2], distance_upper_bound=distance)[0]
        r2 = (x <= distance).sum() / len(target)
        return (precision, recall, p2 - precision, r2 - recall)

def evaluate_result(inference, label, sampling, accept_distance=2.0, threshold=0.6, return_positions=False):
    "Evaluate the prediction for an image."
    distance = int(accept_distance / sampling)
    # Find the peaks, looping over all classes if any.
    numclasses = inference.shape[-1]
    if numclasses > 1:
        numclasses -= 1   # We do not find peaks in background class
        assert numclasses >= 2

    for i in range(numclasses):
        infer_peaks = find_local_peaks(inference[:,:,i], min_distance=distance,
                                       threshold=threshold, exclude_border=10,
                                       exclude_adjacent=True)
        label_peaks = find_local_peaks(label[:,:,i], min_distance=distance,
                                       threshold=threshold, exclude_border=10,
                                       exclude_adjacent=True)

        # Refine the peaks
        region = disk(2)
        infer_refined = refine_peaks(normalize(inference[:,:,i]), infer_peaks,
                                    region, model='polynomial')
        label_refined = refine_peaks(normalize(label[:,:,i]), label_peaks,
                                    region, model='polynomial')
        if len(infer_refined) == 0:
            infer_refined = np.zeros((0,2))
        if len(label_refined) == 0:
            label_refined = np.zeros((0,2))
        if numclasses > 1:
            # Add a z-coordinate, the class times a large number (separating them for peak-matching)
            infer_refined = np.concatenate((infer_refined, 100*i*np.ones([len(infer_refined), 1])), axis=1)
            label_refined = np.concatenate((label_refined, 100*i*np.ones([len(label_refined), 1])), axis=1)
        if i == 0:
            infer_total = infer_refined
            label_total = label_refined
        else:
            infer_total = np.concatenate((infer_total, infer_refined))
            label_total = np.concatenate((label_total, label_refined))
    result = precision_recall(infer_total, label_total, sampling)
    if return_positions:
        return result + (infer_total, label_total)
    else:
        return result 

